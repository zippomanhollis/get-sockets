import os
from flask import Flask

app = Flask(__name__)
@app.route('/')
def helloWorld():
    return "<h1 style=\"color:red\">Hello World!</h1> Welcome to the website about stuff! <p>Find out about <a href=\"/about\">me</a>"

@app.route('/about')
def aboutMe():
    return "<h1>About me me me</h1> I was born on a farm in Wisconsin.<p><a href=\"/\">home</a>"
    
if __name__ == '__main__':
    app.run(host = os.getenv('IP', '0.0.0.0'), port = int(os.getenv('PORT', '8080')), debug = True)