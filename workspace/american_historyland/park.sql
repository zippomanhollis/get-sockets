-- create and change to the park database
DROP DATABASE IF EXISTS park;
CREATE DATABASE  park;
\c park;

CREATE EXTENSION pgcrypto;

-- create bot
DROP ROLE IF EXISTS ahpbot;
CREATE ROLE ahpbot WITH LOGIN;

-- create activities table
DROP TABLE IF EXISTS activities;
CREATE TABLE activities (
  id serial NOT NULL,
  name varchar(35)  NOT NULL,
  location varchar(40) NOT NULL,
  cost numeric(5,2) NOT NULL,
  event_date date NOT NULL,
  event_time time NOT NULL);

-- dump entries
INSERT INTO activities (name, location, cost, event_date, event_time) VALUES ('Picnic', 'The Triangle', 20, '2017-2-14', '4:00 PM');
INSERT INTO activities (name, location, cost, event_date, event_time) VALUES ('Bonfire', 'The Burning Pile', 30, '2017-5-12', '5:30 PM');
INSERT INTO activities (name, location, cost, event_date, event_time) VALUES ('Cowboy Show', 'The shooting rannge', 30, '2017-6-12', '14:30');

-- grant permissions
GRANT SELECT, INSERT ON activities TO ahpbot;
GRANT SELECT, UPDATE ON activities_id_seq TO ahpbot;

-- create residents table
DROP TABLE IF EXISTS residents;
CREATE TABLE residents (
  id serial NOT NULL,
  tenant_setup_key text NOT NULL,
  name varchar(35)  NOT NULL,
  sex varchar(10),
  username varchar(20),
  password varchar(100),
  monthly_bill numeric(5,2),
  bill_due date);
  
-- dump entries
INSERT INTO residents (name, sex, username, password, tenant_setup_key) VALUES ('John Smith', 'male', 'jsmith', crypt('jsmith', gen_salt('bf')), 'jsmith05271992');
INSERT INTO residents (name, sex, username, password, tenant_setup_key) VALUES ('Jane Corbin', 'female', 'jcorbin', crypt('jcorbin', gen_salt('bf')), 'jcorbin03241992');
INSERT INTO residents (name, tenant_setup_key) VALUES ('Johny Appleseed', 'jappleseed01012000');
INSERT INTO residents (name, tenant_setup_key) VALUES ('Jim Jones', 'jjones01012000');
INSERT INTO residents (name, tenant_setup_key) VALUES ('Margaret Pultz', 'mpultz03241992');

--GRANT SELECT(name, username, password, tenant_setup_key, monthly_bill, bill_due), UPDATE(username, password) ON residents TO ahpbot;
GRANT SELECT, UPDATE(username, password, sex) ON residents TO ahpbot;
GRANT SELECT, UPDATE ON residents_id_seq TO ahpbot;

-- create residents table
DROP TABLE IF EXISTS stores;
CREATE TABLE stores (
  id serial NOT NULL,
  name varchar(35)  NOT NULL,
  sells_clothes boolean NOT NULL,
  hours varchar(35)  NOT NULL,
  distance numeric(3,1)  NOT NULL,
  address varchar(100)  NOT NULL,
  sex varchar(10)  NOT NULL);

  
-- dump entries
INSERT INTO stores (name, sells_clothes, sex, hours, distance, address) VALUES ('Walmart', true, 'uni', '24 Hrs', 11.7, '125 Washington Square Plaza, Fredericksburg, VA 22405');
INSERT INTO stores (name, sells_clothes, sex, hours, distance, address) VALUES ('Food Lion', false, 'uni', '7:00 AM to 10:00 PM', 7.0, '8149 Kings Hwy, King George, VA 22485');
INSERT INTO stores (name, sells_clothes, sex, hours, distance, address) VALUES ('Dress Barn', true, 'female', '9:00 AM to 7:30 PM', 15.4, 'Central Park, 1370 Carl D. Silver Parkway, Fredericksburg, VA 22401');
INSERT INTO stores (name, sells_clothes, sex, hours, distance, address) VALUES ('Victoria''s Secret', true, 'female', '9:00 AM to 8:00 PM', 15.3, 'Spotsylvania Towne Center, 810 Spotsylvania Mall, 810, Fredericksburg, VA 22407');
INSERT INTO stores (name, sells_clothes, sex, hours, distance, address) VALUES ('Men''s Wearhouse', true, 'male', '8:00 AM to 7:00 PM', 15.3, 'Central Park, 1210 Carl D. Silver Parkway, Fredericksburg, VA 22401');
INSERT INTO stores (name, sells_clothes, sex, hours, distance, address) VALUES ('Jos. A. Bank', true, 'male', '9:00 AM to 8:30 PM', 15.0, 'Central Park, 1460 Central Park Blvd #114, Fredericksburg, VA 22401');
INSERT INTO stores (name, sells_clothes, sex, hours, distance, address) VALUES ('Macy''s', true, 'uni', '9:00 AM to 8:00 PM', 15.9, 'Spotsylvania Towne Center, 3102 Plank Road, Fredericksburg, VA 22407');
INSERT INTO stores (name, sells_clothes, sex, hours, distance, address) VALUES ('Harbor Freight', false, 'uni', '8:00 AM to 7:00 PM', 14.0, '2011 Plank Rd, Fredericksburg, VA 22401');
INSERT INTO stores (name, sells_clothes, sex, hours, distance, address) VALUES ('Starbucks', false, 'uni', '24 Hrs', 13.8, 'Westwood Center LLC, 2001 Plank Rd, Fredericksburg, VA 22401');
INSERT INTO stores (name, sells_clothes, sex, hours, distance, address) VALUES ('Best Buy', false, 'uni', '9:00 AM to 9:00 PM', 15.6, 'Central Park, 1541 Carl D. Silver Parkway, Fredericksburg, VA 22401');
INSERT INTO stores (name, sells_clothes, sex, hours, distance, address) VALUES ('Kohls', true, 'uni', '8:30 AM to 9:30 PM', 15.6, 'Central Park, 1571 Carl D. Silver Parkway, Fredericksburg, VA 22401');
INSERT INTO stores (name, sells_clothes, sex, hours, distance, address) VALUES ('Lowes', false, 'uni', '6:00 AM to 8:00 PM', 15.3, 'Central Park, 1361 Carl D. Silver Parkway, Fredericksburg, VA 22401');
INSERT INTO stores (name, sells_clothes, sex, hours, distance, address) VALUES ('Graniger''s', false, 'uni', '9:00 AM to 5:00 PM', 17.8, 'First right off Harrison Rd.');
INSERT INTO stores (name, sells_clothes, sex, hours, distance, address) VALUES ('Books-a-Million', false, 'uni', '8:00 AM to 10:00 PM', 15.8, 'Spotsylvania Towne Center, 137 Spotsylvania Mall Dr, Fredericksburg, VA 22407');
INSERT INTO stores (name, sells_clothes, sex, hours, distance, address) VALUES ('Guitar Center', false, 'uni', '9:00 AM to 8:00 PM', 15.4, 'Spotsylvania Towne Center, 215 Spotsylvania Mall Dr, Fredericksburg, VA 22407');
INSERT INTO stores (name, sells_clothes, sex, hours, distance, address) VALUES ('Good Will', true, 'uni', '8:00 AM to 7:00 PM', 14.4, 'Fredericksburg Design Center, 2336 Plank Rd, Fredericksburg, VA 22401');
INSERT INTO stores (name, sells_clothes, sex, hours, distance, address) VALUES ('Sheetz', false, 'uni', '24 Hrs', 14.3, '2807 Lafayette Blvd, Fredericksburg, VA 22408');
INSERT INTO stores (name, sells_clothes, sex, hours, distance, address) VALUES ('Staples', false, 'uni', '9:00 AM to 8:30 PM', 13.8, '2003 Plank Rd, Fredericksburg, VA 22401');

--GRANT SELECT(name, username, password, tenant_setup_key, monthly_bill, bill_due), UPDATE(username, password) ON residents TO ahpbot;
GRANT SELECT ON stores TO ahpbot;
GRANT SELECT ON stores_id_seq TO ahpbot;

-- create residents table
DROP TABLE IF EXISTS chat;
CREATE TABLE chat (
  id serial NOT NULL,
  name varchar(35)  NOT NULL,
  text text  NOT NULL);

INSERT INTO chat (name, text) VALUES ('Bot A', 'trying to get running');
INSERT INTO chat (name, text) VALUES ('Bot B', 'Good luck');

GRANT SELECT, INSERT ON chat TO ahpbot;
GRANT SELECT, UPDATE ON chat_id_seq TO ahpbot;


\password ahpbot
