import psycopg2
import psycopg2.extras
import os
from lib.config import *
from lib import data_postgresql as pg
from flask import Flask, redirect, render_template, request, session

# r4
from flask_socketio import SocketIO, emit
import uuid
#import sys
#reload(sys)
#sys.setdefaultencoding("UTF8")



app = Flask(__name__, static_url_path='/static')
#app.secret_key = os.urandom(24).encode('hex')
app.secret_key = os.urandom(24).hex()
app.config['SECRET_KEY'] = 'secret!'


# r4
socketio = SocketIO(app)


username=''
password=''

services = [{"code" : "trs", "name" : "Trash Collection", "price" : "INCLUDED"},
        {"code" : "lm", "name" : "Lawn Mowing", "price" : "$30 - $60 (determined by lot size and condition)"},
        {"code" : "lr", "name" : "Leaf Raking", "price" : "$50 - $80 (determined by lot size and condition)"},
        {"code" : "wtr", "name" : "Water Supply", "price" : "$3 - $20 (based on monthly usage)"},
        {"code" : "drsnw", "name" : "Driveway and Walkway Snow Removal", "price" : "$40 - $90 (determined by area to be cleared)"},
        {"code" : "plw", "name" : "Snow Plowing of Park Roads", "price" : "INCLUDED"}]

# r1
@app.route('/')
def mainIndex():
    if 'name' in session:
        name = session['name']
    else:
        name = ''
    return render_template('index.html', name=name, services=services, selected="home")

# r1
@app.route('/about')
def aboutIndex():
    if 'name' in session:
        name = session['name']
    else:
        name = ''
    return render_template('about.html', name=name, services=services, selected="about")

# r1
@app.route('/rentals')
def rentalsIndex():
    if 'name' in session:
        name = session['name']
    else:
        name = ''
    trailers = [{"address" : "8024 Park Ridge Road", "size" : "14ft x 70ft", "price" : "$600/month", "available" : True},
            {"address" : "3626 Dogwood Court", "size" : "14ft x 60ft", "price" : "$720/month", "available" : False}]
    return render_template('rentals.html', name=name, services=services, trailers=trailers, selected="rentals")

# r1
@app.route('/services')
def servicesIndex():
    if 'name' in session:
        name = session['name']
    else:
        name = ''
    return render_template('services.html', name=name, services=services, selected="services")

# r2
@app.route('/activities', methods=['GET', 'POST'])
def activitiesIndex():
    if 'name' in session:
        name = session['name']
    else:
        name = ''
    if request.method == 'POST':
        result = pg.addEvent(request.form['name'],
            request.form['location'],
            request.form['cost'],
            request.form['event_date'],
            request.form['event_time'])
        # if database did not accept the query
        if result == None:
            return render_template('donebroke.html', name=name, services=services, selected="activities")
    # get the activities from the database
    results = pg.getAllEvents()
    print(results)
    # if unable to load from database
    if results == None:
        return render_template('donebroke.html', name=name, services=services, selected="activities")
    # all went well    
    return render_template('activities.html', activities=results, name=name, services=services, selected="activities")

# r3
@app.route('/account', methods=['GET', 'POST'])
def accountIndex():
    message = ''
    if request.method == 'POST':
        name = ''
        session['username'] = request.form['username']
        session['password'] = request.form['password']
        result = pg.login(session['username'], session['password'])
        if result == None:
            session.pop('username', None)
            session.pop('password', None)
            return render_template('donebroke.html', name=name, services=services, selected="activities")
        if result == 'ERROR':
            username = session['username']
            session.pop('username', None)
            session.pop('password', None)
            message = 'user name does not exist or does not match password, please try again'
            return render_template('account.html', name=name, username=username, message=message, services=services, selected="account")
        else:
            session['name'] = result

    if 'name' in session:
        name = session['name']
    else:
        name = ''
    return render_template('account.html', name=name, message=message, services=services, selected="account")

# r3
@app.route('/logout', methods=['GET'])
def logoutIndex():
    session.pop('name', None)
    session.pop('username', None)
    session.pop('password', None)
    name = ''
    return mainIndex()

# r3
@app.route('/activate_account', methods=['GET', 'POST'])
def activateAccountIndex():
    message = ''
    if request.method == 'POST':
        name = ''
        session['username'] = request.form['username']
        session['password'] = request.form['password']
        print(request.form['sex'])
        session['verify_password'] = request.form['verify_password']
        session['key'] = request.form['key']
        session['sex'] = request.form['sex']
        if session['password'] == '' or session['verify_password'] == '':
            message = 'Passwords must not be empty'
            username = session['username']
            key = session['key']
            session.pop('password', None)
            session.pop('verify_password', None)
            return render_template('activate_account.html', name=name, username=username, key=key, message=message, services=services, selected="account")
        if session['password'] != session['verify_password']:
            message = 'Passwords must match'
            username = session['username']
            key = session['key']
            session.pop('password', None)
            session.pop('verify_password', None)
            return render_template('activate_account.html', name=name, username=username, key=key, message=message, services=services, selected="account")
        result = pg.addUser(session['username'], session['password'], session['key'], session['sex'])
        session.pop('verify_password', None)
        if result == None:
            session.pop('username', None)
            session.pop('password', None)
            session.pop('key', None)
            return render_template('donebroke.html', name=name, services=services, selected="activities")

        username = session['username']
        key = session['key']
        if result == 'INVALID_KEY':
            session.pop('password', None)
            message = "This account setup key is invalid. Please contact the office if you believe you have received this message in error."
            return render_template('activate_account.html', name=name, username=username, key=key, message=message, services=services, selected="account")
        elif result == 'ALREADY_ACTIVATED':
            session.pop('password', None)
            message = "This account has already been activated. Please return to the account login page to login to your account."
            return render_template('activate_account.html', name=name, username=username, key=key, message=message, services=services, selected="account")
        elif result == 'INVALID_USERNAME':
            session.pop('password', None)
            message = "This username has already been taken. Please select another username."
            return render_template('activate_account.html', name=name, username=username, key=key, message=message, services=services, selected="account")
        else:
            result = pg.login(session['username'], session['password'])
            session['name'] = result
            session.pop('password', None)
            session.pop('key', None)
            session.pop('sex', None)
            message = 'You have succesfully made your account!'
            name = session['name']
            return render_template('account.html', name=name, message=message, services=services, selected="account")

    if 'name' in session:
        name = session['name']
    else:
        name = ''
    return render_template('activate_account.html', name=name, message=message, services=services, selected="account")

# r3
@app.route('/search', methods=['GET', 'POST'])
def searchIndex():
    if 'name' in session:
        name = session['name']
    else:
        name = ''
    if request.method == 'POST':
        session['search']=request.form['search']
        if 'username' in session:
            results = pg.search(session['username'], session['search'])
        else:
            results = pg.search('', session['search'])
        if results == None:
            session.pop('search', None)
            return render_template('donebroke.html', name=name, services=services, selected="search")
        search = session['search']
        session.pop('search', None)
        message = ''
        if not results:
            message = 'No stores found'
        return render_template('search.html', name=name, stores=results, message=message, search=search, services=services, selected="search")

    return render_template('search.html', name=name, services=services, selected="search")

# r4
@app.route('/test', methods=['GET'])
def testIndex():
    if 'name' in session:
        name = session['name']
    else:
        name = ''
        return accountIndex()
    print('in hello world')
    return render_template('test.html', name=name, services=services, selected="account")


# r4
messages = [{'text' : 'Booting System', 'name' : 'Bot'},
    {'text' : 'ISS Chat online', 'name' : 'Bot'}]

chatters = {}


# r4
#@socketio.on('connect', namespace = '/ahp')
@socketio.on('connect')
def makeConnection():
    print("in the connect method")
    session['uuid'] = uuid.uuid1()
    session['chatname'] = 'New user'
    print('connected')
    #chatters[session['uuid']] = {'chatname' : 'New user'}
    chatters[session['uuid']] = {'chatname' : session['name']}
    chats = pg.getChats()

    for chat in chats:
        print(chat)
        print(chat['name'])
        print(chat['text'])
        #emit('message', chat)
        emit('message', {'name': chat['name'], 'text':chat['text']})

    for message in messages:
        print(message)
#        emit('message', message)

#@socketio.on('message', namespace='/ahp')
@socketio.on('message')
def new_message(message):
    print('in new message')
    tmp = {'text' : message, 'name' : chatters[session['uuid']]['chatname']}
    print("got the message")
    print(tmp)
    messages.append(tmp)
    print("added the message to the message list")
    pg.addChat(tmp['name'], tmp['text'])
    print("sent to database")
    emit('message', tmp, broadcast=True)

#@socketio.on('identify', namespace='/ahp')
#def on_identify(message):
#    print('identify ' + message)
#    #chatters[session['uuid']] = {'chatname': message}













#start the server
if __name__ == '__main__':
    # uncomment to run on google cloud or mac
    #socketio.run(app, host='0.0.0.0', port=80, debug=True)
    # uncomment to run on vagrant
    socketio.run(app, host=os.getenv('IP', '0.0.0.0'), port=int(os.getenv('PORT', 8080)), debug=True)
