--
-- PostgreSQL database dump
--

-- Dumped from database version 9.5.5
-- Dumped by pg_dump version 9.5.5

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


--
-- Name: pgcrypto; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS pgcrypto WITH SCHEMA public;


--
-- Name: EXTENSION pgcrypto; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION pgcrypto IS 'cryptographic functions';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: activities; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE activities (
    id integer NOT NULL,
    name character varying(35) NOT NULL,
    location character varying(40) NOT NULL,
    cost numeric(5,2) NOT NULL,
    event_date date NOT NULL,
    event_time time without time zone NOT NULL
);


ALTER TABLE activities OWNER TO postgres;

--
-- Name: activities_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE activities_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE activities_id_seq OWNER TO postgres;

--
-- Name: activities_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE activities_id_seq OWNED BY activities.id;


--
-- Name: users; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE users (
    id integer NOT NULL,
    tenant_setup_key text NOT NULL,
    name character varying(35) NOT NULL,
    username character varying(20),
    password character varying(100),
    monthly_bill numeric(5,2),
    bill_due date
);


ALTER TABLE users OWNER TO postgres;

--
-- Name: users_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE users_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE users_id_seq OWNER TO postgres;

--
-- Name: users_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE users_id_seq OWNED BY users.id;


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY activities ALTER COLUMN id SET DEFAULT nextval('activities_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY users ALTER COLUMN id SET DEFAULT nextval('users_id_seq'::regclass);


--
-- Data for Name: activities; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY activities (id, name, location, cost, event_date, event_time) FROM stdin;
1	Picnic	The Triangle	20.00	2017-02-14	16:00:00
2	Bonfire	The Burning Pile	30.00	2017-05-12	17:30:00
3	Cowboy Show	The shooting rannge	30.00	2017-06-12	14:30:00
4	Concert	Back Circle	20.00	2016-07-15	11:00:00
9	bar-b-que	front lawn	15.00	2017-07-11	11:00:00
10	Cookie Exchange	The Office	10.00	2017-12-01	17:00:00
16	Craft day	The circle	22.00	2017-02-21	10:30:00
\.


--
-- Name: activities_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('activities_id_seq', 16, true);


--
-- Data for Name: users; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY users (id, tenant_setup_key, name, username, password, monthly_bill, bill_due) FROM stdin;
1	jsmith05271992	John Smith	jsmith	$2a$06$CCYLOrhkozQgYm0DBg9MnOFpXVMDdZw40jT30NRt3f3ayyLeF50fm	\N	\N
2	jcorbin03241992	Joe Corbin	jcorbin	$2a$06$rCy1zlqwNcy.8haH6ZlTMeaqLCyGycdduQU/p98SGZ60tVe8rCNhS	\N	\N
3	jappleseed01012000	Johny Appleseed	\N	\N	\N	\N
4	jjones01012000	Jim Jones	jjones	$2a$06$Q8Nr5hRfVqe1/zhfPaTwOO1aHMfyPfy.X7MokPmJSrX7YvVhR9CRW	\N	\N
\.


--
-- Name: users_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('users_id_seq', 4, true);


--
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- Name: activities; Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL ON TABLE activities FROM PUBLIC;
REVOKE ALL ON TABLE activities FROM postgres;
GRANT ALL ON TABLE activities TO postgres;
GRANT SELECT,INSERT ON TABLE activities TO ahpbot;


--
-- Name: activities_id_seq; Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL ON SEQUENCE activities_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE activities_id_seq FROM postgres;
GRANT ALL ON SEQUENCE activities_id_seq TO postgres;
GRANT SELECT,UPDATE ON SEQUENCE activities_id_seq TO ahpbot;


--
-- Name: users; Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL ON TABLE users FROM PUBLIC;
REVOKE ALL ON TABLE users FROM postgres;
GRANT ALL ON TABLE users TO postgres;
GRANT SELECT ON TABLE users TO ahpbot;


--
-- Name: users.username; Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL(username) ON TABLE users FROM PUBLIC;
REVOKE ALL(username) ON TABLE users FROM postgres;
GRANT UPDATE(username) ON TABLE users TO ahpbot;


--
-- Name: users.password; Type: ACL; Schema: public; Owner: postgres
--

REVOKE ALL(password) ON TABLE users FROM PUBLIC;
REVOKE ALL(password) ON TABLE users FROM postgres;
GRANT UPDATE(password) ON TABLE users TO ahpbot;


--
-- PostgreSQL database dump complete
--

