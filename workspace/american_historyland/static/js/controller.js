"use strict";

var AHPchat = angular.module('AHPchat', []);

AHPchat.controller('ChatController', function($scope) {
    //uncomment to run on google cloud
    //var socket = io.connect('http://' + document.domain + ':80');
    //uncomment to run on vagrant
    var socket = io.connect('http://' + document.domain + ':8080');
    //var socket = io.connect('https://' + document.domain + ':' + location.port + '/ahp');

    $scope.messages = [];
    $scope.name = '';
    $scope.text = '';

    socket.on('message', function(msg){
        console.log(msg);
        $scope.messages.push(msg);
        $scope.$apply();
        var elem = document.getElementById('msgpane');
        elem.scrollTop = elem.scrollHeight;
        
    });

    $scope.send = function send(){
        console.log('Sending message: ', $scope.text);
        socket.emit('message', $scope.text);
        $scope.text = '';
    };

//    $scope.setName = function setName(){
//        console.log('Sending name: ', $scope.name);
//        socket.emit('identify', $scope.name)
//    };

    socket.on('connect', function(){
        console.log('connected');
    });

});
