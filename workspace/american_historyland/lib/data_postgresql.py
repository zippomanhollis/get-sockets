#########################################################################
# Hollis Pultz ##########################################################
# CPSC_350 ##############################################################
# American Historyland postresql accessor file ##########################
#########################################################################

from datetime import datetime
import psycopg2
import psycopg2.extras
from psycopg2 import connect
from psycopg2.extras import RealDictCursor

from lib.config import *

'''
The section with general database accessor code is highly influenced by the demonstration
code presented by Professor Zacharski; this is due to the fact that his code shows a very 
nice and straight forward way of accessing and querying the data base.  I have studied this
code and believe to understand all its functions thoroughly.  No code was "copied" as in 
drag-n-drop, however some has been typed character for character so as to have a better 
understand of it and familiarity with it.  All code has been heavily commented to help show
understanding of what is happening with each line.
'''

# open a connection to the database using the information from lib.config
def connectToPostgres():
    # the string of information to connect to the database
    connectString = "dbname=%s user=%s password=%s host=%s" % (POSTGRES_DATABASE, POSTGRES_USER, POSTGRES_PASSWORD, POSTGRES_HOST)
    print("The connection string is as follows: " + connectString)

    try:
        # returns a connection to the database; called by other methods of this class
        return psycopg2.connect(connectString)
    except Exception as failure:
        # print of what type failure is, and its message then return `None`
        print(type(failure))
        print(failure)
        print("Unable to connect to database")
        return None

# perform a query on the database using the supplied arguments.
# defaults to search query unless `select` is specified `false` and `args` are given
def executeQuery(query, conn, select=True, args=None):
    print("Executing query")
    # obtain cursor
    cursor = conn.cursor(cursor_factory=RealDictCursor)
    #cursor = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
    results = None

    # attempt to query the database
    try:
        # get the query to be sent
        quer = cursor.mogrify(query, args).decode('utf-8')
        print(quer)
        # send the query
        cursor.execute(quer)

        # if the query is a search and should return data
        if select:
            results = cursor.fetchall()
        conn.commit()
    except Exception as failure:
        # print of what type failure is, and its message then return `None`
        print("type<")
        print(type(failure))
        print(">type")
        print(failure)
    # dispose of the cursor when finished with it
    cursor.close()
    return results

#########################################################################
# Specific functions for `park` database ################################
#########################################################################

# add an activity to the database
def addEvent(name, location, cost, event_date, event_time):
    # open connection to the database
    conn = connectToPostgres()
    # if database did not connect successfully
    if conn == None:
        return None

    # contruct the bare string for the query
    query = "INSERT INTO activities (name, location, cost, event_date, event_time) VALUES (%s, %s, %s, %s, %s)"
    # send query to general method
    results = executeQuery(query, conn, False, (name, location, cost, event_date, event_time))
    # finished with connection to database
    conn.close()
    # all is well, return with code 0
    return 0

#return a list of python dictionaries of the activities in the database
def getAllEvents():
    # open connection to the database
    conn = connectToPostgres()
    # if database did not connect successfully
    if conn == None:
        return None

    # contruct the bare string for the query
    query = "SELECT name, location, cost, event_date, to_char(event_time, 'HH12:MI:AM') FROM activities"
    #query = "SELECT name, location, cost, event_date, event_time FROM activities"
    # send query to general method
    result = executeQuery(query, conn)
    # finished with connection to database
    conn.close()
    # all is well, return with the results of the query
    return result

# r3
def login(username, password):
    # open connection to the database
    conn = connectToPostgres()
    # if database did not connect successfully
    if conn == None:
        return None

    query = "SELECT name FROM residents WHERE username = %s AND password = crypt(%s, password);"

    result = executeQuery(query, conn, True, (username, password))
    # finished with connection to database
    conn.close()
    if not result:
        print(result)
        return 'ERROR'
    # all is well, return with the results of the query
    return result[0]['name']

# r3
def addUser(username, password, key, sex):
    # open connection to the database
    conn = connectToPostgres()
    # if database did not connect successfully
    if conn == None:
        return None
    query = "SELECT * FROM residents WHERE tenant_setup_key = %s;"
    result = executeQuery(query, conn, True, (key,))
    # finished with connection to database
    conn.close()
    if not result:
        return "INVALID_KEY"

    conn = connectToPostgres()
    query = "SELECT username FROM residents WHERE tenant_setup_key = %s;"
    result = executeQuery(query, conn, True, (key,))
    # finished with connection to database
    conn.close()
    print(result)
    print(result[-1])
    print(type(result[-1]))
    print(type(result))
    if result[-1]['username'] != None:
        return "ALREADY_ACTIVATED"


    conn = connectToPostgres()
    query = "SELECT * FROM residents WHERE username = %s;"
    result = executeQuery(query, conn, True, (username,))
    # finished with connection to database
    conn.close()
    if result:
        return "INVALID_USERNAME"
    else:
        conn = connectToPostgres()
        query = "UPDATE residents SET username = %s, password = crypt(%s, gen_salt('bf')), sex = %s WHERE tenant_setup_key = %s;"   
        result = executeQuery(query, conn, False, (username, password, sex, key))
        conn.close()
        return "SUCCESS"

    return 0

# r3
def search(username, find):
    # open connection to the database
    conn = connectToPostgres()
    # if database did not connect successfully
    if conn == None:
        return None

    if 'clothes' in find.lower() or 'clothing' in find.lower():
        if username == '':
            query = "SELECT * FROM stores WHERE sells_clothes = true"
            result = executeQuery(query, conn)
        else:
            query = "SELECT sex FROM residents WHERE username = %s;"
            result = executeQuery(query, conn, True, (username,))
            if find.lower().strip() in ['clothes', 'clothing']:
                query = "SELECT * FROM stores WHERE sells_clothes = true AND (sex = %s OR sex = 'uni')"
                result = executeQuery(query, conn, True, (result[0]['sex'],))
            else:
                query = "SELECT * FROM stores WHERE sells_clothes = true AND (sex = %s OR sex = 'uni' OR LOWER(name) LIKE %s)"
                result = executeQuery(query, conn, True, (result[0]['sex'], ("%" + find.replace('clothes', '').replace('clothing', '').strip().lower() + "%")))

    elif find.lower().strip() == 'all':
        query = "SELECT * FROM stores"
        result = executeQuery(query, conn)

    else:
        query = "SELECT * FROM stores WHERE LOWER(name) LIKE %s"
        result = executeQuery(query, conn, True, (("%" + find.strip().lower() + "%"),))
        # finished with connection to database
    conn.close()
    return result


# add a chat to the database
def addChat(name, message):
    # open connection to the database
    conn = connectToPostgres()
    # if database did not connect successfully
    print("in add chat")
    if conn == None:
        return None

    # contruct the bare string for the query
    query = "INSERT INTO chat (name, text) VALUES (%s, %s)"
    print("heres the query")
    print(query)
    # send query to general method
    results = executeQuery(query, conn, False, (name, message))
    print("sent the query")
    # finished with connection to database
    conn.close()
    # all is well, return with code 0
    return 0

#return a searched chat
def searchChat(finding):
    # open connection to the database
    conn = connectToPostgres()
    # if database did not connect successfully
    if conn == None:
        return None

    # contruct the bare string for the query
    query = "SELECT name, text FROM chat WHERE name LIKE %s OR message LIKE %s"
    # send query to general method
    result = executeQuery(query, conn, True, (finding,))
    # finished with connection to database
    conn.close()
    # all is well, return with the results of the query
    return result


#return a list of chats
def getChats():
    print("got into the chat collection")
    # open connection to the database
    conn = connectToPostgres()
    print("connected to postgres")
    # if database did not connect successfully
    if conn == None:
        return None

    # contruct the bare string for the query
    query = "SELECT name, text FROM chat"
    # send query to general method
    result = executeQuery(query, conn)
    print("sent the query")
    print(result)
    # finished with connection to database
    conn.close()
    print("closed the database")
    # all is well, return with the results of the query
    return result







