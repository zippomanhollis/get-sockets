# To get socket running on google cloud  

### If you were having issues running socketio on vagrant, these commands appear solve those as well  
  
### _This repo is posted on BitBucket under my honor that I am only helping people to learn how to get socket io running on google per Dr. Zacharski's request. This is not for people to treat as a template for their rudiments, but as a learning supplement to help them understand the methods Zack and I used for getting sockets working._  
  
### _Tested for creating a virtual machine from scratch and cloning the repo, works great_  
  
### _NOTE: this has been tested on ubuntu 16.10 with python3_  
  
### _IF YOU ARE USING A_ `micro` _INSTANCE SWITCH TO AT LEAST A_ `g1-small` _OR THIS WILL CRASH_   
  
### new format for supervisor control  
* change the second line to update the gunicorn command  
* change the fifth    
```
[program:uni_app]
command = gunicorn -k gunicorn.workers.ggevent.GeventWorker -b :80 server:app
autostart=true
autorestart=true
directory=/home/your_home_directory/your_project_name
stderr_logfile=/var/log/uni.err.log
stdout_logfile=/var/log/uni.out.log
```

### install python3 setup tools  
`sudo apt-get install python3-setuptools`

### install python3's pip  
`sudo easy_install3 pip`

### installing more stuff  
`sudo pip3 install gevent gevent-websocket flask-socketio`

### uninstall eventlet  
`sudo pip3 uninstall eventlet`

### nuke the old guincorn and install python3's  
```
sudo pip install gunicorn
sudo pip uninstall gunicorn
sudo pip3 install gunicorn
```

### install greelet (may already be installed)  
`pip3 install greenlet`

### install python3's psycopg2  
`sudo apt-get install python3-psycopg2`

### stuff in the code  
* if you have these lines, delete them
```python
import sys
reload(sys)
sys.setdefaultencoding("UTF8")
```
  
* if you have this line   
```
app.secret_key = os.urandom(24).encode('hex')
```
* change it to this. encode is python2 syntax  
```
app.secret_key = os.urandom(24).hex()
```
  
* remove the namespaces from your server.py file, for example  
```python
@socketio.on('message', namespace='/ahp')
```
* becomes  
```python
@socketio.on('message')
```
  
* use this line for the run line  
```python
socketio.run(app, host='0.0.0.0', port=80, debug=True)
```
* not this one  
```python
socketio.run(app, host=os.getenv('IP', '0.0.0.0'), port=int(os.getenv('PORT', 8080)), debug=True)
```
  
* change this line in your controller.js  
```js
var socket = io.connect('https://' + document.domain + ':' + location.port + '/whatever_your_namespace');
```
* to  
```
var socket = io.connect('http://' + document.domain + ':80');
```
  
* open postres and type `\i park.sql` to setup the park database  
    * use `asdfghjkl` as the password  
  
### reread, update and restart supervisor  
```
sudo supervisorctl reread
sudo supervisorctl update
sudo supervisorctl restart all
```
